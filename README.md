# ZeroCraft-Client

#### 介绍

ZeroCraft服务器客户端[Bug反馈](https://gitee.com/McAZero/zerocraft-client/issues)、[建议反馈](https://gitee.com/McAZero/zerocraft-client/issues)渠道。

#### 软件架构

Java版

#### 安装教程

- *.zip请使用解压软件解压。
- *.exe请在Windows系统双击安装。

#### 使用说明

1. 进入文件夹双击"开始游戏.exe"。
2. 耐心等待加载完成。

#### 下载地址

http://pan.baidu.com/s/1i4I58nN